#pragma once
#include "afxcmn.h"
#include "ClosureState.h"


// StateDlg 대화 상자입니다.

class CClosureStateDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CClosureStateDlg)

public:
	CClosureStateDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CClosureStateDlg();
	CClosureState* m_csSet;
	inline void InitDatabase(CClosureState* csSet) {
		m_csSet = csSet;
	}
// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_STATE_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	bool _inited;
	DECLARE_MESSAGE_MAP()
public:
	void UpdateDBView();
	CListCtrl m_DBView;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedLoad();
};
