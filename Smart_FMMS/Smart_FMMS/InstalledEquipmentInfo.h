// InstalledEquipmentInfo.h : CInstalledEquipmentInfo의 선언입니다.

#pragma once

// 코드 생성 위치 2018년 11월 25일 일요일, 오전 1:39

class CInstalledEquipmentInfo : public CRecordset
{
public:
	CInstalledEquipmentInfo(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CInstalledEquipmentInfo)

// 필드/매개 변수 데이터

// 아래의 문자열 형식(있을 경우)은 데이터베이스 필드의 실제 데이터 형식을
// 나타냅니다(CStringA:ANSI 데이터 형식, CStringW: 유니코드 데이터 형식).
// 이것은 ODBC 드라이버에서 불필요한 변환을 수행할 수 없도록 합니다.  // 원할 경우 이들 멤버를 CString 형식으로 변환할 수 있으며
// 그럴 경우 ODBC 드라이버에서 모든 필요한 변환을 수행합니다.
// (참고: 유니코드와 이들 변환을 모두 지원하려면  ODBC 드라이버
// 버전 3.5 이상을 사용해야 합니다).

	long	m_Index;	//일련번호
	CStringW	m_LocationName;	//함체위치 명, 예)관양2동 복지센터앞
	CStringW	m_EquipmentType;	//장비 Type, 예)IP Camera
	CStringW	m_State;	//상태, 예)정상동작
	CStringW    m_Department;
	CStringW	m_Manufacturer;	//제조사 명, 예)삼성테크윈
	CStringW	m_ModelName;	//모델명, 예)SNP-5300H
	CStringW	m_IPAddress;	//IP주소, 예)192.168.0.27
	long	m_Port;	//포트번호, 예)8080
	CStringW	m_Password;	//비밀번호, 예)pass
	CStringW	m_InstallationCompany;	//설치업체
	CStringW	m_MaintenanceCompany;	//유지보수업체
	CStringW	m_CompanyTelephone;	//업체연락처
	BOOL	m_Availability;	//사용여부

// 재정의
	// 마법사에서 생성한 가상 함수 재정의
	public:
	virtual CString GetDefaultConnect();	// 기본 연결 문자열

	virtual CString GetDefaultSQL(); 	// 레코드 집합의 기본 SQL
	virtual void DoFieldExchange(CFieldExchange* pFX);	// RFX 지원

// 구현
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

};


