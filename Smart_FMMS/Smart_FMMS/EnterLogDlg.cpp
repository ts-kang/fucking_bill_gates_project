// EnterLogDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Smart_FMMS.h"
#include "EnterLogDlg.h"
#include "afxdialogex.h"


// CEnterLogDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CEnterLogDlg, CDialogEx)

CEnterLogDlg::CEnterLogDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_ENTERLOG_DIALOG, pParent)
	, _inited(false)
{

}

CEnterLogDlg::~CEnterLogDlg()
{
}

void CEnterLogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DBVIEW, m_DBView);
}


BEGIN_MESSAGE_MAP(CEnterLogDlg, CDialogEx)
END_MESSAGE_MAP()


// CEnterLogDlg 메시지 처리기입니다.


void CEnterLogDlg::UpdateDBView()
{
	static short int width = 100;
	static short int width_open = 150;
	static LPCTSTR name[] = 
	{ _T("그룹이름1"),_T("그룹이름2"),_T("함체위치명"),_T("출입자명"),
		_T("Open Door1") ,_T("Open Door2"),_T("Close Door1"),_T("Close Door2")};
	if (!_inited) {
		for (int i = 0; i < 8; i++) {
			m_DBView.InsertColumn(i + 1, name[i], LVCFMT_LEFT, i<4?width:width_open);
		}
		_inited = 1;
	}
	m_DBView.DeleteAllItems();
	CEnterLog entlog(m_entSet->m_pDatabase);
	entlog.Open(CRecordset::dynaset, entlog.GetDefaultSQL());
	for (int i = 0; !entlog.IsEOF(); entlog.MoveNext(), i++) {
		m_DBView.InsertItem(i, entlog.m_GroupName1);
		m_DBView.SetItemText(i, 1, entlog.m_GroupName2);
		m_DBView.SetItemText(i, 2, entlog.m_LocationName);
		m_DBView.SetItemText(i, 3, entlog.m_PasserName);
		CString itemStr;
		if (entlog.m_OpenDoor1.GetTime() > 0) {
			itemStr.Format(_T("%d-%d-%d %02d:%02d:%02d"),
				entlog.m_OpenDoor1.GetYear(),
				entlog.m_OpenDoor1.GetMonth(),
				entlog.m_OpenDoor1.GetDay(),
				entlog.m_OpenDoor1.GetHour(),
				entlog.m_OpenDoor1.GetMinute(),
				entlog.m_OpenDoor1.GetSecond());
			m_DBView.SetItemText(i, 4, itemStr);
		}
		if (entlog.m_OpenDoor2.GetTime() > 0) {
		itemStr.Format(_T("%d-%d-%d %02d:%02d:%02d"),
			entlog.m_OpenDoor2.GetYear(),
			entlog.m_OpenDoor2.GetMonth(),
			entlog.m_OpenDoor2.GetDay(),
			entlog.m_OpenDoor2.GetHour(),
			entlog.m_OpenDoor2.GetMinute(),
			entlog.m_OpenDoor2.GetSecond());
		m_DBView.SetItemText(i, 5, itemStr);
		}
		if (entlog.m_CloseDoor1.GetTime() > 0) {
		itemStr.Format(_T("%d-%d-%d %02d:%02d:%02d"),
			entlog.m_CloseDoor1.GetYear(),
			entlog.m_CloseDoor1.GetMonth(),
			entlog.m_CloseDoor1.GetDay(),
			entlog.m_CloseDoor1.GetHour(),
			entlog.m_CloseDoor1.GetMinute(),
			entlog.m_CloseDoor1.GetSecond());
		m_DBView.SetItemText(i, 6, itemStr);
		}
		if (entlog.m_CloseDoor2.GetTime() > 0) {
		itemStr.Format(_T("%d-%d-%d %02d:%02d:%02d"),
			entlog.m_CloseDoor2.GetYear(),
			entlog.m_CloseDoor2.GetMonth(),
			entlog.m_CloseDoor2.GetDay(),
			entlog.m_CloseDoor2.GetHour(),
			entlog.m_CloseDoor2.GetMinute(),
			entlog.m_CloseDoor2.GetSecond());
		m_DBView.SetItemText(i, 7, itemStr);
		}
	}
	entlog.Close();
}


BOOL CEnterLogDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	UpdateDBView();
	m_DBView.SetExtendedStyle(LVS_EX_FULLROWSELECT);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
