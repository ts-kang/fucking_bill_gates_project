// EventLog.h : CEventLog 클래스의 구현입니다.



// CEventLog 구현입니다.

// 코드 생성 위치 2018년 11월 25일 일요일, 오전 12:26

#include "stdafx.h"
#include "EventLog.h"
IMPLEMENT_DYNAMIC(CEventLog, CRecordset)

CEventLog::CEventLog(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_GroupName1 = L"";
	m_GroupName2 = L"";
	m_LocationName = L"";
	m_Event = L"";
	m_EventDescription = L"";
	m_OccurrenceDate;
	m_nFields = 6;
	m_nDefaultType = dynaset;
}
// 아래 연결 문자열에 일반 텍스트 암호 및/또는 
// 다른 중요한 정보가 포함되어 있을 수 있습니다.
// 보안 관련 문제가 있는지 연결 문자열을 검토한 후에 #error을(를) 제거하십시오.
// 다른 형식으로 암호를 저장하거나 다른 사용자 인증을 사용하십시오.
CString CEventLog::GetDefaultConnect()
{
	return _T("DSN=Smart_FMMS;DBQ=DatabaseDesignEx.mdb;DriverId=25;FIL=MS Access;MaxBufferSize=2048;PageTimeout=5;UID=admin;");
}

CString CEventLog::GetDefaultSQL()
{
	return _T("[이벤트 로그]");
}

void CEventLog::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// RFX_Text() 및 RFX_Int() 같은 매크로는 데이터베이스의 필드
// 형식이 아니라 멤버 변수의 형식에 따라 달라집니다.
// ODBC에서는 자동으로 열 값을 요청된 형식으로 변환하려고 합니다
	RFX_Text(pFX, _T("[GroupName1]"), m_GroupName1);
	RFX_Text(pFX, _T("[GroupName2]"), m_GroupName2);
	RFX_Text(pFX, _T("[LocationName]"), m_LocationName);
	RFX_Text(pFX, _T("[Event]"), m_Event);
	RFX_Text(pFX, _T("[EventDescription]"), m_EventDescription);
	RFX_Date(pFX, _T("[OccurrenceDate]"), m_OccurrenceDate);

}
/////////////////////////////////////////////////////////////////////////////
// CEventLog 진단

#ifdef _DEBUG
void CEventLog::AssertValid() const
{
	CRecordset::AssertValid();
}

void CEventLog::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG


