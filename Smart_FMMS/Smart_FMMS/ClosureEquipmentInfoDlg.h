#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "ClosureEquipmentInfo.h"
// CClosureEquipmentInfoDlg 대화 상자입니다.

class CClosureEquipmentInfoDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CClosureEquipmentInfoDlg)

public:
	CClosureEquipmentInfoDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CClosureEquipmentInfoDlg();

	CClosureEquipmentInfo* m_ceSet;
	void InitDatabase(CClosureEquipmentInfo* ceSet) {
		m_ceSet = ceSet;
	}
public:
// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CLOSUREEQUIPMENTINFO_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_DBView;
	CEdit m_EquipmentType;
	CEdit m_Manufacturer;
	CEdit m_ModelName;
	CEdit m_Specification;
	void UpdateDBView();
	afx_msg void OnBnClickedLoad();
	virtual BOOL OnInitDialog();
protected:
	bool _inited;
	int m_index;
public:
	afx_msg void OnLvnItemchangedDbview(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedAdd();
	afx_msg void OnBnClickedEdit();
	afx_msg void OnBnClickedDelete();
};
