// ClosureState.h : CClosureState 클래스의 구현입니다.



// CClosureState 구현입니다.

// 코드 생성 위치 2018년 12월 11일 화요일, 오후 3:46

#include "stdafx.h"
#include "ClosureState.h"
IMPLEMENT_DYNAMIC(CClosureState, CRecordset)

CClosureState::CClosureState(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_LocationName = L"";
	m_Door1 = FALSE;
	m_Door2 = FALSE;
	m_TCPIP = FALSE;
	m_RF = FALSE;
	m_Temerature = L"";
	m_Humidity = L"";
	m_Siren = FALSE;
	m_Sensor = FALSE;
	m_nFields = 9;
	m_nDefaultType = dynaset;
}
// 아래 연결 문자열에 일반 텍스트 암호 및/또는 
// 다른 중요한 정보가 포함되어 있을 수 있습니다.
// 보안 관련 문제가 있는지 연결 문자열을 검토한 후에 #error을(를) 제거하십시오.
// 다른 형식으로 암호를 저장하거나 다른 사용자 인증을 사용하십시오.
CString CClosureState::GetDefaultConnect()
{
	return _T("DSN=Smart_FMMS;DBQ=DatabaseDesignEx.mdb;DriverId=25;FIL=MS Access;MaxBufferSize=2048;PageTimeout=5;UID=admin;");
}

CString CClosureState::GetDefaultSQL()
{
	return _T("[함체 상태]");
}

void CClosureState::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// RFX_Text() 및 RFX_Int() 같은 매크로는 데이터베이스의 필드
// 형식이 아니라 멤버 변수의 형식에 따라 달라집니다.
// ODBC에서는 자동으로 열 값을 요청된 형식으로 변환하려고 합니다
	RFX_Text(pFX, _T("[LocationName]"), m_LocationName);
	RFX_Bool(pFX, _T("[Door1]"), m_Door1);
	RFX_Bool(pFX, _T("[Door2]"), m_Door2);
	RFX_Bool(pFX, _T("[TCP/IP]"), m_TCPIP);
	RFX_Bool(pFX, _T("[RF]"), m_RF);
	RFX_Text(pFX, _T("[Temerature]"), m_Temerature);
	RFX_Text(pFX, _T("[Humidity]"), m_Humidity);
	RFX_Bool(pFX, _T("[Siren]"), m_Siren);
	RFX_Bool(pFX, _T("[Sensor]"), m_Sensor);

}
/////////////////////////////////////////////////////////////////////////////
// CClosureState 진단

#ifdef _DEBUG
void CClosureState::AssertValid() const
{
	CRecordset::AssertValid();
}

void CClosureState::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG


